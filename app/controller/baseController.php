<?php
namespace app\controller;
use core\lib\config;

class baseController{
    public $data=[];
    public function assign($name, $value){
        $this->data[$name] = $value;
    }
    public function display($file){
        $loader = new \Twig\Loader\FilesystemLoader(APP.'/view');

        $twig = new \Twig\Environment($loader, [
            'cache' => ROOT.'/'.config::get('path','log').'/compilation_cache',
            'debug' => DEBUG
        ]);
        echo $twig->render($file, $this->data);
    }
    public function error(){
        $loader = new \Twig\Loader\ArrayLoader([
            'index' => 'Hello {{ name }}!',
        ]);
        $twig = new \Twig\Environment($loader);
        echo $twig->render('index', ['name' => 'Fabien']);

        exit();
    }
    public function islogin(){

    }
}