<?php
namespace core\lib;
class model extends \Medoo\Medoo {
    public function __construct()
    {
        $dbcfg = \core\lib\config::get('','database');

        try{
            parent::__construct(
                [
                    'database_type' => 'mysql',
                    'database_name' => $dbcfg['dbname'],
                    'server' => 'localhost',
                    'username' => $dbcfg['username'],
                    'password' => $dbcfg['password'],
                    'charset' => 'utf8'
                ]
            );
        }catch (\PDOException $e){
            p($e->getMessage());
        }
    }
}