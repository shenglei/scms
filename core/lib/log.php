<?php
namespace core\lib;
class log{
    public static $class;
    public static $driver;

    public static function init(){
        $driver = config::get('driver', 'log');
        $className = '\core\lib\driver\log\\'.$driver;
        static::$class = new $className();
        return static::$class;

    }
    public static function write($message, $dir = 'log'){
        $class = static::$class;
        return $class->write($message, $dir);
    }
}