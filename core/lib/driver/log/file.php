<?php
namespace core\lib\driver\log;
use core\lib\config;

class file{
    protected $path;
    public function __construct()
    {
        $this->path = ROOT.'/'.config::get('path', 'log');
    }

    public function write($message, $dir){
        $path = $this->path .'/'. $dir;
        if(!is_dir($path)){
            mkdir($path, '0777', true);
        }
        $file = $path. '/' .date('Ymdh').'.php';
        $start = is_file($file) ? '' : '<?php echo exit();?>'.PHP_EOL;
        $content = $start . date('Y-m-d H:i:s').' '.json_encode($message) . PHP_EOL;
        return file_put_contents($file, $content, FILE_APPEND);
    }
}