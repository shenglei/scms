<?php
namespace core\lib;
class config{
    public static $cfg = [];
    public static function get($name='', $file){
        if(!isset($cfg[$file])){
            $path = CORE.'/config/'.$file.'.php';
            if(!is_file($path)) throw new \Exception('配置文件不存在');
            $cfg = static::$cfg[$file] = include $path;
        }else{
            $cfg = static::$cfg[$file];
        }

        if(empty($name)) return $cfg;
        if(isset($cfg[$name])) return $cfg[$name];
        throw new \Exception('配置不存在');
    }
}