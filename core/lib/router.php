<?php
namespace core\lib;
class router{
    public $ctrl;
    public $act;
    public function __construct()
    {
        if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/'){
            $uri = $_SERVER['REQUEST_URI'];
            $uri = preg_replace('/\/{2,}/','/',$uri);
            $arr = explode('/', trim($uri, '/'));
            if(isset($arr[0]) && $arr[0]){
                $this->ctrl = $arr[0];
                unset($arr[0]);
            }else{
                $this->ctrl = 'index';
            }
            if(isset($arr[1]) && $arr[1]){
                $this->act = $arr[1];
                unset($arr[1]);
            }else{
                $this->act = 'index';
            }
            $len = count($arr)+2;
            $i=2;
            $params = [];
            while ($i <= $len && isset($arr[$i+1])){
                $params[$arr[$i]] = $arr[$i+1];
                $i+=2;
            }
        }else{
            $this->ctrl = $this->act = 'index';
        }
    }
}