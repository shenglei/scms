<?php
namespace core;

class app{
    protected static $classMap = [];
    public static function run(){
        $router = new \core\lib\router();
        $controllerName = $router->ctrl;
        $action = $router->act;
        $controllerFile = APP.'/controller/'.$controllerName.'Controller.php';
        if(is_file($controllerFile)){
            include_once $controllerFile;
            $name = '\\app\\controller\\'.$controllerName.'Controller';
            $controller = new $name();
            $controller->$action();
        } else {
            throw new \Exception('找不到控制器');
        }

    }
    public static function load($class){
        $class = str_replace('\\','/',$class);
        if(isset(static::$class[$class])) return true;
        $path = ROOT .'/'. $class . '.php';
        if(is_file($path)){
            static::$classMap[$class] = $class;
            return include_once $path;
        }
        return false;
    }
}