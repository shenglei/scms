<?php
define('ROOT', dirname(__DIR__));
define('CORE', ROOT. '/core');
define('APP', ROOT.'/app');
define('DEBUG', true);
include ROOT. '/vendor/autoload.php';
if(DEBUG === true){
    $whoops = new \Whoops\Run();
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
    $whoops->register();
    ini_set('display_errors','On');
    ini_set('error_reporting', E_ALL);
}else{
    ini_set('display_errors', 'Off');
}
require_once CORE.'/common/function.php';
include_once CORE.'/app.php';
spl_autoload_register("\core\app::load");
\core\app::run();

\core\lib\log::init();
\core\lib\log::write('testtest');